-- phpMyAdmin SQL Dump
-- version 4.1.6deb0ubuntu1ppa1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-01-31 20:06:35
-- 服务器版本： 5.5.35-MariaDB-1~precise-log
-- PHP Version: 5.4.24-1+sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ccqo`
--
CREATE DATABASE IF NOT EXISTS `ccqo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ccqo`;

-- --------------------------------------------------------

--
-- 表的结构 `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `accounts`
--

INSERT INTO `accounts` (`id`, `created`, `modified`, `username`, `password`) VALUES
(1, NULL, NULL, 'caiknife', 'c4dba9494baa43b818187f60c062f01c');

-- --------------------------------------------------------

--
-- 表的结构 `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `public_date` varchar(50) NOT NULL,
  `is_public` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `title` (`title`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `articles`
--

INSERT INTO `articles` (`id`, `created`, `modified`, `title`, `content`, `category_id`, `public_date`, `is_public`) VALUES
(1, '0000-00-00 00:00:00', '2014-01-31 19:00:56', '中国质量人才网', '', 10101, '2014-01-31', 0),
(2, '0000-00-00 00:00:00', '2014-01-31 19:23:32', '中国质量人才网', '', 10101, '2014-02-12', 1),
(3, '0000-00-00 00:00:00', '2014-01-31 19:23:29', '中国质量人才网', '', 10101, '2014-02-12', 1),
(4, '0000-00-00 00:00:00', '2014-01-31 19:23:27', '中国质量人才网', '', 10101, '2014-02-12', 1),
(5, '0000-00-00 00:00:00', '2014-01-31 19:23:23', '中国质量人才网', '', 10101, '2014-02-12', 1),
(6, '0000-00-00 00:00:00', '2014-01-31 19:23:21', '中国质量人才网', '', 10101, '2014-02-12', 1),
(7, '0000-00-00 00:00:00', '2014-01-31 20:04:00', '中国质量人才网', '<p>\r\n	<a class="ke-insertfile" href="/ccqo/app/webroot/kindeditor/attached/file/20140131/20140131195909_75768.txt" target="_blank">文本</a> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src="/ccqo/app/webroot/kindeditor/attached/image/20140131/20140131195817_16064.jpg" alt="" /> \r\n</p>', 10101, '2014-02-12', 1),
(8, '2014-01-31 18:58:21', '2014-01-31 18:58:24', '', '', NULL, '', 0),
(12, '2014-01-31 20:00:10', '2014-01-31 20:00:10', '', '', NULL, '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;