<?php
const ADMIN_SITE_TITLE = '中国质量人才网后台管理系统';

const FRONT_SITE_TITLE = '中国质量人才网';

const MESSAGE_ITEM_NOT_EXISTS = '该条记录并不存在！';
const MESSAGE_COLUMN_COUNT_NOT_MATCH = 'Excel 表格数据列数匹配异常，请检查后重新上传！';
const MESSAGE_EXCEL_TYPE_INCORRECT = '上传文件不是有效的 Excel 文件！';