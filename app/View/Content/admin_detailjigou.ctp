<div class="container-fluid">
    <div class="row-fluid">
        <label class="span6">组织机构代码</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['zuzhijigoudaima']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">名称</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['name']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">所属省市</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['suoshushengshi']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">推荐机构</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['tuijianjigou']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">备案号</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['beianhao']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">类型</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['leixing']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">联系人</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['lianxiren']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">联系电话</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['lianxidianhua']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">邮箱</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['youxiang']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">备案时间</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['beianshijian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">地址</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['dizhi']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">邮编</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['youbian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">评测结果</label>
        <label class="span6">
            <?php echo h($item['TrainDivision']['pingcejieguo']);?>
        </label>
    </div>
</div>