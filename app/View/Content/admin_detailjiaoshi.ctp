<div class="container-fluid">
    <div class="row-fluid">
        <label class="span6">名称</label>
        <label class="span6">
            <?php echo h($item['Trainer']['name']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">照片</label>
        <label class="span6">
            <?php echo h($item['Trainer']['zhaopian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">身份证号</label>
        <label class="span6">
            <?php echo h($item['Trainer']['identity']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">性别</label>
        <label class="span6">
            <?php echo h($item['Trainer']['gender']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">地区</label>
        <label class="span6">
            <?php echo h($item['Trainer']['diqu']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">职务</label>
        <label class="span6">
            <?php echo h($item['Trainer']['zhiwu']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">职称</label>
        <label class="span6">
            <?php echo h($item['Trainer']['zhicheng']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">单位</label>
        <label class="span6">
            <?php echo h($item['Trainer']['danwei']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训课程</label>
        <label class="span6">
            <?php echo h($item['Trainer']['peixunkecheng']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">专业</label>
        <label class="span6">
            <?php echo h($item['Trainer']['zhuanye']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">成果</label>
        <label class="span6">
            <?php echo h($item['Trainer']['chengguo']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">入库时间</label>
        <label class="span6">
            <?php echo h($item['Trainer']['rukushijian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">教师简介</label>
        <label class="span6">
            <?php echo h($item['Trainer']['jiaoshijianjie']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">联系电话</label>
        <label class="span6">
            <?php echo h($item['Trainer']['lianxidianhua']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">手机</label>
        <label class="span6">
            <?php echo h($item['Trainer']['shouji']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">邮箱</label>
        <label class="span6">
            <?php echo h($item['Trainer']['youxiang']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训次数</label>
        <label class="span6">
            <?php echo h($item['Trainer']['peixuncishu']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">评测结果</label>
        <label class="span6">
            <?php echo h($item['Trainer']['pingcejieguo']);?>
        </label>
    </div>
</div>