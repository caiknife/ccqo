<div class="container-fluid">
    <div class="row-fluid">
        <label class="span6">证书编号</label>
        <label class="span6">
            <?php echo h($item['Trainee']['zhengshubianhao']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">学员编号</label>
        <label class="span6">
            <?php echo h($item['Trainee']['xueyuanbianhao']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">姓名</label>
        <label class="span6">
            <?php echo h($item['Trainee']['name']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">身份证号</label>
        <label class="span6">
            <?php echo h($item['Trainee']['identity']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">性别</label>
        <label class="span6">
            <?php echo h($item['Trainee']['gender']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">通讯地址</label>
        <label class="span6">
            <?php echo h($item['Trainee']['dizhi']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">所属行业</label>
        <label class="span6">
            <?php echo h($item['Trainee']['suoshuhangye']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">发证机构</label>
        <label class="span6">
            <?php echo h($item['Trainee']['fazhengjigou']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">发证时间</label>
        <label class="span6">
            <?php echo h($item['Trainee']['fazhengshijian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">所在单位</label>
        <label class="span6">
            <?php echo h($item['Trainee']['suozaidanwei']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">职务</label>
        <label class="span6">
            <?php echo h($item['Trainee']['zhiwu']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">职称</label>
        <label class="span6">
            <?php echo h($item['Trainee']['zhicheng']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">任职时间</label>
        <label class="span6">
            <?php echo h($item['Trainee']['renzhishijian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">工作电话</label>
        <label class="span6">
            <?php echo h($item['Trainee']['gongzuodianhua']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">手机</label>
        <label class="span6">
            <?php echo h($item['Trainee']['shouji']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">邮箱</label>
        <label class="span6">
            <?php echo h($item['Trainee']['youxiang']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">继续教育学时</label>
        <label class="span6">
            <?php echo h($item['Trainee']['jixujiaoyuxueshi']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">是否参加评测</label>
        <label class="span6">
            <?php echo h($item['Trainee']['shifoucanjiapingce']);?>
        </label>
    </div>
</div>
