<div class="container-fluid">
    <div class="row-fluid">
        <label class="span6">培训机构备案号</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunjigoubeianhao']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训时间</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunshijian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训机构</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunjigou']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训地点</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixundidian']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训人数</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunrenshu']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训费用</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunfeiyong']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">注重行业</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['zhuzhonghangye']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训师资</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunshizi']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训通知</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixuntongzhi']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">培训课程</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['peixunkecheng']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">主要内容</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['zhuyaoneirong']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">关联文章</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['guanlianwenzhang']);?>
        </label>
    </div>
    <div class="row-fluid">
        <label class="span6">备注</label>
        <label class="span6">
            <?php echo h($item['TrainInfo']['beizhu']);?>
        </label>
    </div>
</div>