<footer class="row-fluid container bottom">
    <div class="span2"></div>
    <div class="span5">
        <p>版权所有：中国质量人才网 京ICP备00000000号</p>
        <p>网站管理：中国质量人才网</p>
        <p>地址：中国北京XXXX街XX号 100000</p>
    </div>
    <div class="span5">
        <p>技术支持：XX有限公司</p>
        <p>技术支持电话：86-10-00000000 传真：86-10-00000000</p>
        <p>业务咨询电话：86-10-00000000 邮箱：XXX@XXX.COM</p>
    </div>
</footer>